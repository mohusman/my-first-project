
import { View, Text,StyleSheet,Image, TouchableOpacity } from 'react-native'
import React from 'react'

const Screen1 = ({navigation}) => {
  return (
    <View>
      <View style={{alignItems:'center',justifyContent:'center',    backgroundColor:'mediumturquoise',}}>
      <Text style={styles.text}>MEDICAL</Text>
      </View>
      <Image style={styles.img} source={require('../image/i.jpg')}>
      </Image>
      <TouchableOpacity onPress={() => navigation.navigate('screen2') }>
        <Text style={styles.text1}>MEDECIN</Text>
      </TouchableOpacity>
    </View>
  )
};
const styles=StyleSheet.create({
  text:{
    fontSize:24,
    textAlignVertical:'center',
    backgroundColor:'mediumturquoise',
    textAlign:'center',
    color:'snow',
    height:40
  },
  img:{
    width:170,
    height:100,
    marginVertical:23,
    marginHorizontal:15,
  },
  text1:{
    fontWeight:'bold',
    fontSize:18,
    marginHorizontal:53,
    top:-20,
    

  }
});

export default Screen1