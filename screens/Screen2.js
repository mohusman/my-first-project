import { View, Text,StyleSheet,FlatList,TouchableOpacity,ScrollView } from 'react-native'
import React from 'react'

const DATA =[
   {  id:'1',
      title:'Dr Authier Bouchenaki Lepide Brun',
      dis:'Distance: 2.3 Km',
      sub:'10 Rue du Couvent',
      ey:'13430 EYGUIERES'
   },
   {
      id:'2',
      title:'Dr Schir Jean Luc',
      dis:'Distance: 3.1 Km',
      sub:'624 Avenue du General de Gaulle',
      ey:'13430 EYGUIERES'
   },
   {
      id:'3',
      title:'Alexandre Brigitte Marie Claude',
      dis:'Distance: 2.2 Km',
      sub:'51 Avenue Gabriel Peri',
      ey:'13430 EYGUIERES'
   },
   {
      id:'4',
      title:'Dr Soissons Clement',
      dis:'Distance: 6.2 Km',
      sub:'Chateau Lamanon',
      ey:'13113 LAMANON'
   },
   {
      id:'5',
      title:'Dr Plesant Jacques',
   },
];


const Screen2 = ({navigation}) => {
  
   const renderDATA = ({item}) => (
   
      <View style={styles.item1}>
         <TouchableOpacity onPress={() => navigation.navigate('screen3') }>
         <Text style={styles.title}>{item.title}</Text>
         </TouchableOpacity>
         <Text style={styles.title1}>{item.dis}</Text>
         <Text style={styles.title2}>{item.sub}</Text>
         <Text style={styles.title3}>{item.ey}</Text>
         <View style={{borderBottomWidth:1, marginVertical:9,marginBottom:-5}}>
         </View>
      </View>
      );
  return (
   
  <View>
   
   <Text style={styles.text}>MEDICIN</Text>

   <Text  style={styles.text1}>
      veuillez trouver ci-dessous d'autres specialistes mais sans Reservation en ligne
      </Text>
      <View style={{borderBottomWidth:1,marginHorizontal:13,marginVertical:4,}}>
      </View>
      <ScrollView>
      <View>
         <FlatList data={DATA}
         renderItem={renderDATA}
         />
      </View>
      </ScrollView>
   </View>
   )
};

const styles=StyleSheet.create({

   text:{
      fontSize:24,
      textAlign:'center',
      textAlignVertical:'center',
      backgroundColor:'#48d1cc',
      height:44,
   },
   text1:{
      fontSize:18,
      textAlign:'center',
      marginVertical:18,
   },
   title:{
      fontSize:21,
      marginVertical:2,
      color:'#008080'
   },
   title1:{
      fontSize:17,
      marginVertical:3,
   },
   title2:{
      fontSize:17,
      marginVertical:5

   },
   title3:{
      fontSize:17,
      marginVertical:5,
   },
   item1:{
      padding:10,
      marginHorizontal:2
   }
});

export default Screen2