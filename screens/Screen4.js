import { View, Text,StyleSheet,Image, ScrollView,TouchableOpacity } from 'react-native'
import React from 'react'

const Screen4 = () => {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={{alignItems:'center',justifyContent:'center', backgroundColor:'#2eb0b7',}}>
        <Text style={styles.txt}>COIFFEUR</Text>
      </View>
      <View style={styles.con1}>
        <Image style={styles.img}   source={require("../image/dog.png")}></Image>
   </View>
      <View style={styles.v1}>
        <Text style={styles.txt1}>  coup'stoof</Text>
      </View>
      <View style={{borderBottomWidth:1,marginLeft:87,marginRight:80,marginVertical:15}}></View>
         <View style={{alignItems:'center'}}>
      <Text style={{fontSize:14,marginTop:-8}}>avenue benoit frachon</Text>
      </View>
  <View style={{alignItems:'center'}}>
    <Text style={{fontSize:14}}>13230, port-saint-louis du rhone</Text>
  </View>
  <View style={{alignItems:'center'}}>
    <Text style={{fontSize:14,marginVertical:3}}>Tel:0667866751</Text>
  </View>
  <View style={{borderBottomWidth:1,marginLeft:87,marginRight:80,marginVertical:15}}></View>
  

  <View style={{alignItems:'center'}}>
   
    
    <Text style={{fontSize:19,color:'#d4007c'}}>Prestation Selectionnee</Text>
  </View>

  <View style={{alignItems:'center'}}>
    <Text style={styles.txt2}>COUPE HOMME</Text>
  </View>
  <View style={{alignItems:'center'}}>
    <Text style={{fontSize:19,color:'#d4007c'}}>Date et Heure :</Text>
  </View>
  <View style={{alignItems:'center'}}>
    <Text style={{fontSize:15,marginVertical:10}}>20,jul 2018 a 09:00</Text>
  </View>
  <View style={{ marginVertical:15,alignItems:'center',justifyContent:'center',flexDirection:'row'}}>
    <Text style={styles.txt3}>Prix : </Text>

  <Text style={styles.txt4}>15 </Text>
  
  <Image  style={styles.img1}  source={require("../image/euro.png")}></Image>
  </View>
  <View style={{alignItems:'center'}}>
    <Text style={{fontSize:14,marginVertical:10,color:'#d4007c'}}>Le prix varie en fonction des conditions*</Text>
  </View>
  <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
  <TouchableOpacity style={styles.to1}>
    <Text style={styles.txt5}>Annuler</Text>
  </TouchableOpacity>
  <TouchableOpacity style={styles.to2}>
    <Text style={styles.txt6}>confirmer</Text>
  </TouchableOpacity>
  </View>

  </ScrollView>
  
</View>
  )
};

const styles=StyleSheet.create({
  container:{
    flex:1,
  },
  con1:{
    justifyContent:'center',
    alignItems:'center'
  },
  txt:{
    fontSize:24,
    textAlign:'center',
    textAlignVertical:'center',
    backgroundColor:'#2eb0b7',
    height:40,
    color:'white',
  
  },

   img:{
    marginVertical:23,
    width:90,
    height:110
   },
   txt1:{
    fontSize:23,
    color:'#d4007c',
    alignItems:'center',
    justifyContent:'center',

 
   },
   con2:{
    height:1,
    flex:1,alignSelf:'center',
    backgroundColor:'red'


   },


   v1:{
    alignItems:'center',
   },
   txt2:{
    fontSize:15,
    marginVertical:20
   },
   txt3:{
    fontSize:20,
   color:'#d4007c',
  },
  img1:{
    width:21,
  },
  txt4:{
    fontSize:22,
    fontWeight:'bold',
    color:'#000000'
  },
  txt5:{
    fontSize:18,
    color:'white',
    textAlign:'center'
  },
  to1:{
    backgroundColor:'#d4007c',
    height:33,
    width:174,
    marginRight:13,
    marginLeft:197,
    borderRadius:5,
    marginVertical:25,
    alignItems:'center',
    justifyContent:'center'
  },
  txt6:{
  fontSize:18,
  color:'white',
  textAlign:'center'
  },
  to2:{
  backgroundColor:'#2eb0b7',
  height:33,
  width:174,
  marginRight:190,
  borderRadius:5,
  marginVertical:-205,
  alignItems:'center',
  justifyContent:'center'
},

})
 
export default Screen4